package com.JSPVault.VaultApi;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.JSPVault.utility.KeyFinder;

public class VaultApi {
	private String token;

	public static String search(String a,String b) {
		JSONParser parser = new JSONParser();
		KeyFinder finder = new KeyFinder();
		finder.setMatchKey(b);
		while(!finder.isEnd()){
			try {
				parser.parse(a, finder,true);
			} catch (ParseException e) {
				System.out.println("sono nella catch della search");
				return " ";
			}
			if(finder.isFound()){
				finder.setFound(false);
				System.out.println("found id:");
				System.out.println(finder.getValue());
				return finder.getValue().toString();
			}
		}
		return " ";
	}


	private static String doG(String path, String token) throws ClientProtocolException, IOException{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpGet httpget= new HttpGet("http://127.0.0.1:8200/v1/"+path);
		HttpGet httpget= new HttpGet(path);
		if(!token.equals("")){
			httpget.addHeader("X-Vault-Token",token);
			//httpget.addHeader("X-Vault-Token","02e70312-638c-e6cc-aeeb-a10fd2ebd5a0");
		}
		System.out.println("Executing request " + httpget.getRequestLine());
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					System.out.println(status);
					HttpEntity entity = response.getEntity();
					//modificare la return per essere in linea con tutto
					return entity != null ? EntityUtils.toString(entity) : null;
					//return entity != null ? (Integer.toString(status)+"\n"+EntityUtils.toString(entity)) : Integer.toString(status);
				} else {
					//throw new ClientProtocolException("Unexpected response status: " + status);
					return Integer.toString(status);
				}
			}

		};
		String responseBody = httpclient.execute(httpget, responseHandler);
		System.out.println("----------------------------------------");
		System.out.println(responseBody);

		httpclient.close();
		return responseBody;
	}
	
	
	public static String doP(String path,String token,String body,String application) throws ClientProtocolException, IOException{

		// Trust own CA and all self-signed certs
		//serve SSLContext sslcontext = null;
		//serve sslcontext = SSLContexts.createDefault();
		/*try {
			sslcontext = SSLContexts.custom()
			        .loadTrustMaterial(KeyStore.getInstance("JKS"),new TrustSelfsigned()).build();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			System.out.println("LOOOOOOOOOOL");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.out.println("LOOOOOOOOOOL");
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			System.out.println("LOOOOOOOOOOL");
		} */
		// Allow TLSv1 protocol only
		//serve SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
		//serve         sslcontext,
		//serve         new String[] { "TLSv1.2" },
		//serve        null,
		//serve       SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		//serve CloseableHttpClient httpclient = HttpClients.custom()
		//serve       .setSSLSocketFactory(sslsf)
		//serve       .build();
		//da cancellare
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpPost httppost= new HttpPost("http://127.0.0.1:8200/v1/"+path);
		HttpPost httppost= new HttpPost(path);
		if(!token.equals("")){
			//System.out.println("non ci dovevo entrare");
			httppost.addHeader("X-Vault-Token",token);
			//httppost.addHeader("X-Vault-Token","02e70312-638c-e6cc-aeeb-a10fd2ebd5a0");
		}
		if(!body.equals("")){
			System.out.println("ci dovevo entrare");
			HttpEntity entity = new ByteArrayEntity(body.getBytes("UTF-8"));
			httppost.setEntity(entity);
			/*List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("key", "lol"));
			nvps.add(new BasicNameValuePair("token", "asd"));
			httppost.setEntity(new UrlEncodedFormEntity(nvps));*/
		}
		if(!application.equals("")){
			//System.out.println("non ci dovevo entrare");
			httppost.addHeader("Content-type",application);
		}
		System.out.println("Executing request " + httppost.getRequestLine());
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
					//return entity != null ? (Integer.toString(status)+"\n"+EntityUtils.toString(entity)) : Integer.toString(status);
				} else {
					System.out.println(EntityUtils.toString(response.getEntity()));
					return Integer.toString(status);
				}
			}

		};
		String responseBody = httpclient.execute(httppost, responseHandler);
		System.out.println("----------------------------------------");
		System.out.println(responseBody);
		httpclient.close();
		return responseBody;

	}
	
	
	
	public static String doPTok(String path,String token,String body,String application) throws ClientProtocolException, IOException{

		// Trust own CA and all self-signed certs
		//serve SSLContext sslcontext = null;
		//serve sslcontext = SSLContexts.createDefault();
		/*try {
			sslcontext = SSLContexts.custom()
			        .loadTrustMaterial(KeyStore.getInstance("JKS"),new TrustSelfsigned()).build();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			System.out.println("LOOOOOOOOOOL");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.out.println("LOOOOOOOOOOL");
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			System.out.println("LOOOOOOOOOOL");
		} */
		// Allow TLSv1 protocol only
		//serve SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
		//serve         sslcontext,
		//serve         new String[] { "TLSv1.2" },
		//serve        null,
		//serve       SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		//serve CloseableHttpClient httpclient = HttpClients.custom()
		//serve       .setSSLSocketFactory(sslsf)
		//serve       .build();
		//da cancellare
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpPost httppost= new HttpPost("http://127.0.0.1:8200/v1/"+path);
		HttpPost httppost= new HttpPost(path);
		if(!body.equals("")){
			System.out.println("ci dovevo entrare");
			//HttpEntity entity = new ByteArrayEntity(body.getBytes("UTF-8"));
			//httppost.setEntity(entity);
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("key", application));
			nvps.add(new BasicNameValuePair("token", token));
			httppost.setEntity(new UrlEncodedFormEntity(nvps));
		}

		System.out.println("Executing request " + httppost.getRequestLine());
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
					//return entity != null ? (Integer.toString(status)+"\n"+EntityUtils.toString(entity)) : Integer.toString(status);
				} else {
					System.out.println(EntityUtils.toString(response.getEntity()));
					return Integer.toString(status);
				}
			}

		};
		String responseBody = httpclient.execute(httppost, responseHandler);
		System.out.println("----------------------------------------");
		System.out.println(responseBody);
		httpclient.close();
		return responseBody;
	}
	
	

	public static String doPut(String path,String body,String token) throws ClientProtocolException, IOException{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpput= new HttpPut(path);
		if(!token.equals("")){
			//System.out.println("non ci dovevo entrare");
			//httppost.addHeader("X-Vault-Token",token);
			httpput.addHeader("X-Vault-Token",token);
		}
		httpput.addHeader("Content-type","application/x-www-form-urlencoded");
		//String body="{ \"secret_shares\" : 1, \"secret_threshold\" : 1}";
		System.out.println(body);
		HttpEntity entity = new ByteArrayEntity(body.getBytes("UTF-8"));
		httpput.setEntity(entity);

		/*List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		nvps.add(new BasicNameValuePair("secret_shares", "1"));
		nvps.add(new BasicNameValuePair("secret_threshold", "1"));
		try {
			httpput.setEntity(new UrlEncodedFormEntity(nvps));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("YOLOOOOOOO");
		}*/

		System.out.println("Executing request " + httpput.getRequestLine());
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
					//return entity != null ? (Integer.toString(status)+"\n"+EntityUtils.toString(entity)) : Integer.toString(status);
				} else {
					System.out.println(EntityUtils.toString(response.getEntity()));
					return Integer.toString(status);
				}
			}

		};
		String responseBody = httpclient.execute(httpput, responseHandler);
		System.out.println("----------------------------------------");
		System.out.println(responseBody);
		httpclient.close();
		return responseBody;
	}
	
	
	public static String doToken(String path,String secret,String token,String component){
		String ret="error";
		try {
			doPut(path+"sys/policy/"+component,"{\"rules\": \"path \\\""+secret+"\\\" { policy = \\\"sudo\\\"}\"}",token);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			ret=search(doP(path+"auth/token/create",token,"{ \"policies\":[ \""+component+"\" ] }",""),"client_token");
			//System.out.println(doP(path+"auth/token/create",token,"{ \"policies\":[ \""+component+"\" ] }",""));
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	
	public  void doAuth(String user,String pass) throws ClientProtocolException, IOException{
		String lol=doP("auth/userpass/login/"+user,"",pass,"");
		this.token = this.search(lol,"client_token");
		//this.Token = this.search(lol,"client_token");	
	}
	
	
	public static ArrayList<String> ReadSecret(String path,String token) throws ClientProtocolException, IOException{
		String lol= doG(path,token);
		//return search(lol,"value");
		String value = search(lol,"value");
		String desc = search(lol,"description");
		ArrayList<String> values = new ArrayList<String>();
		values.add(value); 
		values.add(desc);
		return values;
	}
	
	
	public static String WriteSecret(String path,String token,String s,String desc) throws ClientProtocolException, IOException{
		//String lol=doP("secret/"+path,token,"{ \"password\": \"prova\" }","application/json");
		JSONObject p= new JSONObject();
		p.put("description", desc);
		p.put("value", s);
		//String lol=doP(path,token,"{ \"value\": \""+s+"\" }","application/json");
		String lol=doP(path,token,p.toJSONString(),"application/json");
		return lol;
	}
	
	
	public static String trigger(String component,String name) throws ClientProtocolException, IOException{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpPost httppost= new HttpPost("http://127.0.0.1:8200/v1/"+path);
		HttpPost httppost= new HttpPost(component);
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		//esempio add token
		nvps.add(new BasicNameValuePair("path", name));
		//esempio write secret
		//nvps.add(new BasicNameValuePair("path", "yolo"));
		//nvps.add(new BasicNameValuePair("secret", "SEGRETOSEGRETISSIMO"));
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nvps));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Executing request " + httppost.getRequestLine());
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				} else {
					throw new ClientProtocolException("Unexpected response status: " + status);
				}
			}

		};
		String responseBody = httpclient.execute(httppost, responseHandler);
		System.out.println("----------------------------------------");
		System.out.println(responseBody);
		httpclient.close();
		return responseBody;
	}
	

	public static String sendToken(String path,String body,String key,String token){
		String S="{ \"error\": \"generic error\" }";
		try {
			S= doPTok(path,token,body,key);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return S;
	}
	
	
	public static String unseal(String path,String key){
		String returned="{ \"message\" : \"Connection error\" }";
		try {
			String body="{ \"key\" : \""+key+"\" }";
			returned=doPut(path,body,"");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returned;
	}

}