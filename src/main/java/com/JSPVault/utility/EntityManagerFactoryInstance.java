package com.JSPVault.utility;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactoryInstance {

    private static EntityManagerFactory managerFactory;

    public EntityManagerFactory getEntityManagerFactory (String entityName){
        return managerFactory == null ? managerFactory = Persistence.createEntityManagerFactory(entityName): managerFactory;
    }
}