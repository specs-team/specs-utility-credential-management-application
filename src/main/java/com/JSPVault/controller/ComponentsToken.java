package com.JSPVault.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.JSPVault.VaultApi.*;
import com.JSPVault.model.*;

/**
 * classe utile per creare un nuovo token ed assegnarlo al componente
 * @author giancarlo
 *
 */

@Controller
public class ComponentsToken {
	/*@RequestMapping(value = "/CreateToken", method = RequestMethod.GET)
	   public ModelAndView Asecret() {
		   //codice java
	      return new ModelAndView("CreateToken", "command", new Secret()); //mappa la pagina di ritorno
	   }*/
	   @RequestMapping(value = "/component-tokens", method= RequestMethod.POST)
	   public @ResponseBody
	   String Associate(@ModelAttribute("SpringWeb")Token secret, 
	   ModelMap model,HttpServletRequest request) {
		   Properties prop=new Properties();
		   try {
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		   System.out.println("POST RECEIVED");
		   String tokencreated="not";
		   String component=request.getParameter("Component");
		   String path=request.getParameter("path");
		   String token=request.getParameter("token");
			//S=VaultApi.doToken(prop.get("vault_server").toString()+"v1/","secret/owner/Broker/*","25f63606-3b5e-f007-b617-5b5b07e6fe49","Broker");
		   String returnedmessage="{ \"error\" : \"some parameters were null\" } ";
		   if((!component.isEmpty()) && (!path.isEmpty()) && (!token.isEmpty()))
		   tokencreated=VaultApi.doToken(prop.get("vault_server").toString()+"v1/",path,token,component);
		   if((component.equals("owner")) && (!path.isEmpty()) && (!token.isEmpty()) && (!tokencreated.equals("error"))){
		   returnedmessage=VaultApi.sendToken("http://localhost:8080/JSPVault/addToken"," ",component,tokencreated);
		   }
		   else if((!component.isEmpty()) && (!path.isEmpty()) && (!token.isEmpty()) && (!tokencreated.equals("error"))) {
			   returnedmessage=VaultApi.sendToken("http://localhost:8080/Component/addToken"," ",component,tokencreated);
		   }
		   //return new ModelAndView("result","message",asd);
		   return returnedmessage;
	   }
}
