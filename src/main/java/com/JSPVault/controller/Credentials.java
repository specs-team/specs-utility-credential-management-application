package com.JSPVault.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;
import com.JSPVault.VaultApi.*;
import com.JSPVault.utility.*;
import com.JSPVault.model.*;

/**
 * memorizza le credenziali al path dell'owner (definito nel 
 * properties file campo "vault_secret_path")
 * @author giancarlo
 *
 */


@Controller
public class Credentials {

  
   @RequestMapping(value = "/credentials", method= RequestMethod.POST)
   public @ResponseBody
   String Associate(@ModelAttribute("SpringWeb")Token secret, 
   ModelMap model,HttpServletRequest request) {
	   Properties prop=new Properties();
	   try {
		prop.load(Thread.currentThread().
				getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
	} catch (IOException e) {
		e.printStackTrace();
	} 
	   System.out.println("POST RECEIVED");
	   ArrayList<String> asd=new ArrayList<String>();
	   String component=request.getParameter("Component");
	   System.out.println(component);
	   EntityManagerFactory j = new EntityManagerFactoryInstance().getEntityManagerFactory("Segreto");
	   //EntityManagerFactory j=Persistence.createEntityManagerFactory("Segreto");
	   Secret s = j.createEntityManager().find(Secret.class,prop.get("vault_component_name").toString());
	   /*for(EnumEx e : EnumEx.values()){
		   asd.add(e.toString());
		   System.out.println("yolo "+model.get(e.value));
	   }*/
	   //model.addAttribute(asd.get(0), secret.getPath());
	   //System.out.println(secret.getComponent()+" "+secret.getC1()+" "+secret.getC2()+" "+secret.getCn()+" "+secret.getChefOrganizationPrivateKey()+" "+secret.getChefUserPrivateKey()+" "+secret.getProviderUserName()+" "+secret.getProviderUserPassword()+" "+secret.getProviderUserPrivateKey());
	   //passando un ulteriore campo "typecredential" posso evitare di fare tutti i controlli,il type lo si può mettere come primo campo della enum in modo da generare un read only"
	   String returnedmessage="{ \"message\" : \"credentials correctly stored\" }";
	   //String lollone=request.getParameter("ChefOrganizationPrivateKey");
	   Enumeration<String> names = request.getParameterNames();
	   //System.out.println("tokeeeeeeeeeeen "+s.getS());
	   while(names.hasMoreElements()){
	   //System.out.println(names);
	   String param=(String)names.nextElement();
	   if(!param.equals("Component")){
		try {
			String message=VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+"owner/"+component+"/"+param, s.getS(),request.getParameter(param), "");
            	if(message!=null){
            		returnedmessage=message;
            	}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return "{ \"error\" : \"credentials not correctly stored\" }";
		} catch (IOException e) {
			e.printStackTrace();
			return "{ \"error\" : \"credentials not correctly stored\" }";
		}
	   }
	   //names.nextElement();
	   }
	   /*
	   try {
		VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+secret.getComponent()+"/"+"ChefOrganizationPrivateKey", "token", secret.getChefOrganizationPrivateKey(), "yolo");
	} catch (ClientProtocolException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	   try {
		VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+secret.getComponent()+"/"+"ChefUserPrivateKey", "token", secret.getChefUserPrivateKey(), "yolo");
	} catch (ClientProtocolException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	   try {
			VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+secret.getComponent()+"/"+"ProviderUserName", "token", secret.getProviderUserName(), "yolo");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	   try {
			VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+secret.getComponent()+"/"+"ProviderUserPassword", "token", secret.getProviderUserPassword(), "yolo");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	   try {
			VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+secret.getComponent()+"/"+"ProviderUserPrivateKey", "token", secret.getProviderUserPrivateKey(), "yolo");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	   return returnedmessage;
	    
   }




}