package com.JSPVault.controller;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.JSPVault.VaultApi.*;
import com.JSPVault.model.*;

/**
 * classe che notifica la disponibilità di nuove credenziali al componente,
 * e ne scatena il retrieve.
 * @author giancarlo
 *
 */


@Controller
public class Trigger {
	//mappa il comportamento della pagina all'url  localhost:8080/addToken con la get
	/*@RequestMapping(value = "/Trigger", method = RequestMethod.GET)
	   public ModelAndView token() {
		   //codice java eseguito quando fai la get

		   System.out.println("i'm in the get");
		   //ritorno un model and view, cioè ritorno la pagina token.jsp che trovi sotto WEB-INF(è una form html anche a me)
	      //return new ModelAndView("token", "command", new Token());
		   return new ModelAndView("Trigger", "command", new Token());
	   }*/
	//mappa il comportamento della pagina all'url  localhost:8080/addToken con la post
	@RequestMapping(value = "/Trigger", method = RequestMethod.POST,headers="Accept=*/*",produces="application/json")
	//aggiunta per leggere il response body, cioè i parametri passati nella POST
	public @ResponseBody
	//ritorno una stringa! e mi prendo il valore i parametri mettendoli nella mia classe Token
	String addToken(
			@ModelAttribute("SpringWeb")
			Token token, 
			ModelMap model,
			HttpServletRequest request) {
		Properties prop=new Properties();
		try {
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String returnedmessage="{ \"error\" : \"some parameters were null\"";
		if(!request.getParameter("Type").isEmpty()){
			try {
				// vedo se posso prendere il segreto, altrimenti genero msg errore
				VaultApi.trigger("http://localhost:8080/Component/GenericComponentServlet",request.getParameter("Type"));
				returnedmessage="{ \"message\" : \"token correctly sent\" }";
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return returnedmessage;
	}
}
