package com.JSPVault.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.JSPVault.VaultApi.*;

/**
 * GET -> init del vault server
 * POST -> unseal del Vault Server, ovvero il processo per renderlo available e,
 * se è la prima volta costruisce la chiave per cifrare i dati e mettere il server in stato available;
 * altrimenti ricarica i dati precedenemtente salvati sul backend per la durability.
 * @author giancarlo
 *
 */

@Controller
public class CredentialsManager {
	/*@RequestMapping(value = "/Init", method = RequestMethod.GET)
	   public ModelAndView Asecret() {
		   //codice java
	      return new ModelAndView("Init", "command", new Token()); //mappa la pagina di ritorno
	   }*/
	   @RequestMapping(value = "/credentials-manager", method= RequestMethod.GET)
	   public @ResponseBody
	   /*ModelAndView Initialize(@ModelAttribute("SpringWeb")Token secret, 
	   ModelMap model) {*/
	   String Initialize() {
		   Properties prop=new Properties();
		   try {
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		   System.out.println("POST RECEIVED");
		   String returnedmessage="{ \"error\" : \"bla bla bla\" }";
		   try {
			returnedmessage=VaultApi.doPut(prop.get("vault_server").toString()+"v1/sys/init","{ \"secret_shares\" : 1, \"secret_threshold\" : 1}","");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		   //return new ModelAndView("result","message",returnedmessage);
		   return returnedmessage;
	   }
	   @RequestMapping(value = "/credentials-manager", method= RequestMethod.POST)
	   public @ResponseBody
	   /*ModelAndView Associate(@ModelAttribute("SpringWeb")Token secret, 
	   ModelMap model,HttpServletRequest request) {*/
	   String Associate(HttpServletRequest request) {
		   Properties prop=new Properties();
		   try {
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		} 
		   System.out.println("POST RECEIVED");
		   String key=request.getParameter("key");
		   //System.out.println(component);
		  String returnedmessage="";
		   if(key!=null){
		   returnedmessage=VaultApi.unseal(prop.get("vault_server").toString()+"v1/sys/unseal",key);
		   //return new ModelAndView("result", "message","lol");
		   }
		   else {
			   returnedmessage="{ \"error\" : \"key parameter requested as response body\"";
		   }
		   return returnedmessage;
		    
	   }
}
