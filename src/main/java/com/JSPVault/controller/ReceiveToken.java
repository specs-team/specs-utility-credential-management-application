package com.JSPVault.controller;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;
import com.JSPVault.VaultApi.*;
import com.JSPVault.utility.*;
import com.JSPVault.model.*;

/**
 * classe che riceve il token e lo salva in persistenza
 *
*/

@Controller
public class ReceiveToken {
   //mappa il comportamento della pagina all'url  localhost:8080/addToken con la get
   
	//@RequestMapping(value = "/addToken", method = RequestMethod.GET,headers="Accept=*/*",produces="application/json")
  /* public ModelAndView token() {
	   //codice java eseguito quando fai la get
	   
	   System.out.println("i'm in the get");
	   //ritorno un model and view, cioè ritorno la pagina token.jsp che trovi sotto WEB-INF(è una form html anche a me)
      //return new ModelAndView("token", "command", new Token());
	   return new ModelAndView("Interface", "command", new Token());
   }*/
 //mappa il comportamento della pagina all'url  localhost:8080/addToken con la post
   @RequestMapping(value = "/addToken", method = RequestMethod.POST,headers="Accept=*/*",produces="application/json")
   //aggiunta per leggere il response body, cioè i parametri passati nella POST
   public @ResponseBody
   //ritorno una stringa! e mi prendo il valore i parametri mettendoli nella mia classe Token
   String addToken(@ModelAttribute("SpringWeb")Token token, 
   ModelMap model) {
	   //le properties per ora non ti interessano quindi vai alla riga 46
	   Properties prop=new Properties();
	   try {
		prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
	} catch (IOException e) {
		e.printStackTrace();
	}
	   
	   System.out.println(prop.get("vault_server").toString());
	   System.out.println("i'm in the post");
     //metto i parametri nell'oggetto token, tu ti crei la classe Dati che avrà 3 attributi(la mia ne ha 2) così come ho creato io la classe token
	  model.addAttribute("key", token.getKey());
      model.addAttribute("token", token.getToken());
      System.out.println("this is the tokeeeeen *********** "+ token.getKey() +" "+token.getToken());
      //System out dei parametri che mi sono stati passati attraverso la POST
      String returnedmessage="{ \"error\" : \"some parameters were null\" }";
      if((!token.getKey().isEmpty()) && (!token.getToken().isEmpty())){
      System.out.println(token.getKey()+" "+token.getToken());
      //salta, questo è il salvataggio in RAM della roba ed a te non interessa, salta alla riga 65
      Secret s= new Secret();
      s.setId(token.getKey());
      s.setS(token.getToken());
      System.out.println(token.getKey());
      System.out.println(token.getToken());
      EntityManagerFactory j = new EntityManagerFactoryInstance().getEntityManagerFactory("Segreto");
      //EntityManagerFactory j=Persistence.createEntityManagerFactory("Segreto");
      EntityManager em =  j.createEntityManager();
      EntityTransaction t = em.getTransaction();
      t.begin();
      em.persist(s);
      t.commit();
      em.close();
      returnedmessage="{ \"message\" : \"token received\" }";
      }
      //faccio un nuovo oggetto json mettendo le informazioni e le ritorno come stringa
      //JSONObject nnn=new JSONObject();
      //nnn.put(token.getKey(), token.getToken());
      
      return returnedmessage;
   }
}
