package com.JSPVault.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;
import java.lang.reflect.Method;
import com.JSPVault.utility.*;
import com.JSPVault.VaultApi.*;
import com.JSPVault.model.*;
import org.apache.logging.log4j.core.*;
import com.google.gson.Gson;


/**
 * copia le credenziali in VAULT dal path dell'owner (definito nel 
 * properties file campo "vault_secret_path") 
 * a quello del componente. cercare nel codice di questa classe
 * @author Giancarlo
 *
 */

@Controller
public class ComponentsCredentials {

	/*@RequestMapping(value = "/Associate", method = RequestMethod.GET)
	public ModelAndView Asecret() {
		//codice java
		return new ModelAndView("Associate", "command", new Token()); //mappa la pagina di ritorno
	}*/
	@RequestMapping(value = "/components-credentials", method= RequestMethod.POST)
	public @ResponseBody
	String Associate(@ModelAttribute("SpringWeb")Token secret, 
			ModelMap model,HttpServletRequest request) {
		System.out.println("Associate method");
		Properties prop=new Properties();
		try {
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		} 
		System.out.println("POST RECEIVED");
		try {
			System.out.println(request.getReader().readLine());
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		BufferedReader brr = null;
		try {
			brr = request.getReader();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		//		String tempvalue2="";
		//		String line2;
		//		try {
		//			while (( line2 = brr.readLine()) != null) {
		//				tempvalue2+=(line2+"\n");
		//			}
		//		} catch (IOException e2) {
		//			e2.printStackTrace();
		//		}
		//
		//		try {
		//			brr.close();
		//		} catch (IOException e2) {
		//			e2.printStackTrace();
		//		}
		//
		//		System.out.println(tempvalue2);
		//		ArrayList<String> asd=new ArrayList<String>();
		String component=request.getParameter("Component");
		//		String temp=request.getParameter("IeatCluster");
		//		System.out.println(temp);
		System.out.println("Component: "+component);
		/*for(EnumEx e : EnumEx.values()){
		   asd.add(e.toString());
		   System.out.println("yolo "+model.get(e.value));
	   }*/
		//model.addAttribute(asd.get(0), secret.getPath());
		//System.out.println(secret.getComponent()+" "+secret.getC1()+" "+secret.getC2()+" "+secret.getCn()+" "+secret.getChefOrganizationPrivateKey()+" "+secret.getChefUserPrivateKey()+" "+secret.getProviderUserName()+" "+secret.getProviderUserPassword()+" "+secret.getProviderUserPrivateKey());
		//passando un ulteriore campo "typecredential" posso evitare di fare tutti i controlli,il type lo si può mettere come primo campo della enum in modo da generare un read only"
		//		String S="lol";

		EntityManagerFactory jj = new EntityManagerFactoryInstance().getEntityManagerFactory("Segreto");
		//EntityManagerFactory jj=Persistence.createEntityManagerFactory("Segreto");
		Secret s = jj.createEntityManager().find(Secret.class, prop.get("vault_component_name").toString());
		//String lollone=request.getParameter("ChefOrganizationPrivateKey");
		Enumeration<String> names = request.getParameterNames();
		Map<String,String[]> parameters=request.getParameterMap();
		String message=null;
		//System.out.println(hh.get("Component")[0]);
		//	   ComponentCredentialsExample hhh=ComponentCredentialsExample.getIstance();
		/*
		BufferedReader br = null;
		ComponentCredentialsExample hhh=null;
		try {
			br = new BufferedReader(new InputStreamReader (Trynna2.class.getClassLoader().getResourceAsStream("credentials2.json"), "UTF8"));
			String tempvalue="";
			String line;
			while (( line = br.readLine()) != null) {
				tempvalue+=(line+"\n");
			}
			br.close();

			hhh = new Gson().fromJson(tempvalue, ComponentCredentialsExample.class);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 


		for(int i=0;i<hhh.getCredentials().size();i++){
			if((hh.get(hhh.getCredentials().get(i).getName()))!=null){
				System.out.println("for "+hhh.getCredentials().get(i).getName());
				for(int j=0;j<hhh.getCredentials().get(i).getKeys().size();j++){
					try {
						String seg=VaultApi.ReadSecret(prop.get("vault_secret_path").toString()+"owner/"+hhh.getCredentials().get(i).getName()+"/"+hhh.getCredentials().get(i).getKeys().get(j).getName(), s.getS()).get(0);
						VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+"owner/"+component+"/"+hhh.getCredentials().get(i).getName()+"/"+hhh.getCredentials().get(i).getKeys().get(j).getName(), s.getS(), seg, "ffff");
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		}
		//names.nextElement();
		//return new ModelAndView("result", "message", S);

		return "{ \"message\" : \"ok\" , \"asd\" : \"lol\" }"; */

		String returnedMsg = "{\"message\":\"credentials correctly assigned\"}";
		while(names.hasMoreElements()){
			String param=(String)names.nextElement();
			if(!param.equals("Component")){ 
				Class<?> c=null;
				try {
					System.out.println("eu.specs.datamodel.credentials."+component+"."+param);
					c=Class.forName("eu.specs.datamodel.credentials."+component+"."+param);
				} catch (ClassNotFoundException e) {
					message= "{\"error\" : \"provider not assignable\" }";
					return message;
				}
				List<Method> templist=Arrays.asList(c.getDeclaredMethods());
				Iterator iter= templist.iterator();



				while(iter.hasNext()){
					String field=iter.next().toString();
					String[] cutfields=field.split("\\.");
					String cutfield=cutfields[cutfields.length-1];
					if(cutfield.startsWith("get")){
						System.out.println("ci sto");
						String realfields= cutfield.substring(3);
						String[] splitting= realfields.split("\\(");
						String realField=splitting[0];
						realField=realField.substring(0, 1).toLowerCase()+realField.substring(1,realField.length());
						String seg=null;
						System.out.println(prop.get("vault_secret_path").toString()+"owner/"+param+"/"+realField);
						System.out.println(prop.get("vault_secret_path").toString()+"owner/"+component+"/"+param+"/"+realField);
						try {
							String path = prop.get("vault_secret_path").toString()+"owner/"+param+"/"+realField;
							s=new Secret();
							//TODO capire perchè il token non viene salvato in persistenza
							s.setS("cf2b7d53-ca63-38d7-6887-f9216b00744a");
							
							if(s!=null){
								String secretValue = s.getS();
								ArrayList<String> arrayList = VaultApi.ReadSecret(path, secretValue);
								if(arrayList!=null && arrayList.size()>0)
									seg = arrayList.get(0);
							}
							else{
								System.out.println("S è null");
							}

						} catch (ClientProtocolException e) {
							return "{ \"error\" : \"credentials can't be found\" }";
						} catch (IOException e) {
							return "{ \"error\" : \"credentials can't be found\" }";
						}
						try {
							String msg = VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+"owner/"+component+"/"+param+"/"+realField, s.getS(), seg, "");
							if(msg!=null){
								returnedMsg = msg;
							}
						} catch (ClientProtocolException e) {
							return "{ \"error\" : \"credentials can't be stored at component path\" }";
						} catch (IOException e) {
							return "{ \"error\" : \"credentials can't be stored at component path\" }";
						}

					}
				}
			}
		}
		return returnedMsg;

	}

	//	public static Set<String> findAllPackagesStartingWith(String prefix) {
	//		List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
	//		classLoadersList.add(ClasspathHelper.contextClassLoader());
	//		classLoadersList.add(ClasspathHelper.staticClassLoader());
	//		Reflections reflections = new Reflections(new ConfigurationBuilder()
	//		.setScanners(new SubTypesScanner(false), new ResourcesScanner())
	//		.setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
	//		.filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(prefix))));
	//		Set<Class<? extends Object>> classes = reflections.getSubTypesOf(Object.class);
	//
	//		Set<String> packageNameSet = new TreeSet<String>();
	//		for (Class classInstance : classes) {
	//			String packageName = classInstance.getPackage().getName();
	//			if (packageName.startsWith(prefix)) {
	//				packageNameSet.add(packageName);
	//			}
	//		}
	//		return packageNameSet;
	//	} 


}
