package com.JSPVault.model;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Segreto
 *
 */
@Entity

public class Secret implements Serializable {

	
	private String secret;   
	
	@Id
	private String id;
	private static final long serialVersionUID = 1L;

	public Secret() {
		super();
	}   
	public String getS() {
		return this.secret;
	}

	public void setS(String S) {
		this.secret = S;
	}   
	public String getId() {
		return this.id;
	}

	public void setId(String Id) {
		this.id = Id;
	}
   
}
