# README #

This README would normally document whatever steps are necessary to get your Credential Management Application up and running.

### What do you need to use the component? ###

* You need to import the following dependencies in your IDE:
    * Specs Utility parent Project (https://bitbucket.org/specs-team/specs-utility-specs_parent) 
* You need to convert the project to a Maven project (if your IDE doesn't recognize it as a Maven one);
* Execute Maven Install. 

### How to use the credential manager? ###

* This component offers a web interface through which it is possible to easily execute all the necessary steps to store new credentials inside the Vault Server.
* The next step is to edit the configuration file (https://bitbucket.org/specs-team/specs-utility-credential-management-application/src/3b4f77060b8beae25c9bc1fbd0391fb6ca35771a/src/main/resources/vault_conf.properties?at=master&fileviewer=file-view-default) to make it compliant with the Vault Server instance.
* The last step consists into running this component so that it's possible to access all the functions developed to interact with the Vault Server and to assigna  new Credential to a specific component. 

### Who do I talk to? ###

* Massimiliano Rak (Cerict) - maxrak@gmail.com