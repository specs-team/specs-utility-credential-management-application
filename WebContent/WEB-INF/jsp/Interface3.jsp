<%@ page language="java" import="java.util.*" import="com.JSPVault.controller.*" import="eu.specs.datamodel.components.ComponentCredentials" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor	</title>
<!-- <link href="/JSPVault/bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet"><!-->
<link href="<c:url value="/bootstrap/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" />

<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body ng-app="ngAnimate">
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="<c:url value="/img/specs-logo-32.png" />"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/Interface2" >Home</a></li>
					<li><a href="<%=request.getContextPath()%>/WriteSecret" >Write a Secret</a></li>
					<li><a href="<%=request.getContextPath()%>/AssociateToComponent" >Associate an existing component</a></li>
					<li><a href="<%=request.getContextPath()%>/Add_Associate" >Write & associate a component</a></li>
					<li><a href="<%=request.getContextPath()%>/ReadSecret" >Read a Secret</a></li>
					<li><a href="<%=request.getContextPath()%>/Trigger" >Trigger a Component</a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>

	
<script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-animate.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<body>
<%
ComponentCredentials hhh=ComponentCredentials.getIstance();
System.out.println("inizio");
System.out.println(hhh.getComponents().get(0).getCredentials().get(0).getName());
System.out.println("fine");
//for(EnumEx.Ex a : EnumEx.Ex.values() ){
//	EnumEx b=new EnumEx(a);
//	System.out.println(a);
	%>
<div class="checkbox">
  <label><input type="checkbox"  ng-model="<%=//a%>"><%=//a %></label>
  <%//ArrayList<EnumSet> n=b.companyDetails();
  //for(int i=0;i<n.size();i++){
	  
	  %><!--  //creareform-->
	  <div class="check-element animate-hide" ng-show="<%=//a%>">
     <form class = "form-horizontal" role = "form" method="POST" action="/JSPVault/Trynna">
	  <div class = "form-group">
      <label for = "key" class = "col-sm-2 control-label"><%=//a%></label>
		
      <div class = "col-sm-10">
         <input type = "text" name="Component"  class = "form-control" value="<%=//a %>" id = "path" placeholder = "Enter Component" readonly >
      </div>
   </div>
	  <%
	 // Object[] arr=n.get(i).toArray();
	 // for(int j=0;j<arr.length;j++){
		  %>
		  <!--  //immettere inputtype-->
		  <div class = "form-group">
      <label for = "key" class = "col-sm-2 control-label"><%=//arr[j]%></label>
		
      <div class = "col-sm-10">
         <input type = "text" name="<%=//arr[j]%>"  class = "form-control" id = "path" placeholder = "Enter <%=//arr[j] %>" >
      </div>
   </div>
		  <%
  		//System.out.println(arr[j]);
  		//System.out.println(j);
	  //}
	  //metterebottone e chiudere form
	  %><!--  //metterebottone e chiudere form-->
	  <div class = "form-group">
      <div class = "col-sm-offset-2 col-sm-10">
         <button type = "submit" value="Submit" class = "btn btn-default">Submit</button>
      </div>
   </div>
	
</form> 
	  <%
 // }
  //chiudere checkbox con div div
  %></div>
  </div><%
//}%>

</body>
</html>