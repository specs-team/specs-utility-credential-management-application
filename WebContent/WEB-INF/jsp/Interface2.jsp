<%@ page language="java" import="java.util.*" import="com.JSPVault.controller.*" import="eu.specs.datamodel.components.ComponentCredentials" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor	</title>
<!-- <link href="/JSPVault/bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet"><!-->
<link href="<c:url value="/bootstrap/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" />

<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->
<%ComponentCredentials hhh=ComponentCredentials.getIstance(); %> 
<body ng-app="ngAnimate">
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="<c:url value="/img/specs-logo-32.png" />"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/Interface2" >Home</a></li>
					<li><a href="<%=request.getContextPath()%>/WriteSecret" >Write a Secret</a></li>
					<li><a href="<%=request.getContextPath()%>/AssociateToComponent" >Associate an existing component</a></li>
					<li><a href="<%=request.getContextPath()%>/Add_Associate" >Write & associate a component</a></li>
					<li><a href="<%=request.getContextPath()%>/Associate" >Read a Secret</a></li>
					<li><a href="<%=request.getContextPath()%>/Trigger" >Trigger a Component</a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>

	<div class="panel">
		<div class="container">
			<h1>Login page to the SPECS SLA-Editor application</h1>
			<h2>Please enter your credentials and select your role</h2>
			
			<form:form method="POST" action="/JSPVault/addToken">
   <table>
    <tr>
        <td><form:label path="key">Key</form:label></td>
        <td><form:input path="key" /></td>
    </tr>
    <tr>
        <td><form:label path="token">Token</form:label></td>
        <td><form:input path="token" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form:form>

		
		
		</div>
	</div>
<script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-animate.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<body>

<div ng-app="">
 
<p>Input something in the input box:</p>
<p>Name: <input type="text" ng-model="name"></p>

<p ng-bind="name"></p>
</div>
<%
    //Secret lol=new Secret();
    //lol.setPath("lol2");
%>
<div class="container">
  <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><p><%="lol" %></li>
      <li><a href="#">CSS</a></li>
      <li><a href="#">JavaScript</a></li>
    </ul>
  </div>
</div>
<div class="checkbox">
  <label><input type="checkbox" ng-model="checked">Option 1</label>
</div>
  <div class="check-element animate-hide" ng-show="checked">
    <div class="form-group">
  <label for="key">Name:</label>
  <input type="text" class="form-control" id="key">
</div>
<div class="form-group">
  <label for="token">Password:</label>
  <input type="text" class="form-control" id="token">
</div>
  </div>
</div>
<div>
  Hide:
  <div class="check-element animate-hide" ng-hide="checked">
    <form:form method="POST" action="/JSPVault/WriteSecret">
   <table>
    <tr>
        <td><form:label path="key">Path</form:label></td>
        <td><form:input path="key" /></td>
    </tr>
    <tr>
        <td><form:label path="token">Key Description</form:label></td>
        <td><form:input path="token" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form:form>
  </div>
</div>
<div class="container">
  <h2>Form control: select</h2>
  <p>The form below contains two dropdown menus (select lists):</p>
  <form role="form">
    <div class="form-group">
      <label for="sel1">Select list (select one):</label>
      <select class="form-control" id="sel1">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
      <br>
      <label for="sel2">Mutiple select list (hold shift to select more than one):</label>
      <select multiple class="form-control" id="sel2">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
      </select>
    </div>
  </form>
</div>
<form class = "form-horizontal" role = "form" method="POST" action="/JSPVault/addToken">
   
   <div class = "form-group">
      <label for = "key" class = "col-sm-2 control-label">First Name</label>
		
      <div class = "col-sm-10">
         <input type = "text" name="token" class = "form-control" id = "firstname" placeholder = "Enter First Name">
      </div>
   </div>
   
   <div class = "form-group">
      <label for = "token" id="key" class = "col-sm-2 control-label">Last Name</label>
		
      <div class = "col-sm-10">
         <input type = "text" name="key" class = "form-control" id = "lastname" placeholder = "Enter Last Name">
      </div>
   </div>
   
   <div class = "form-group">
      <div class = "col-sm-offset-2 col-sm-10">
         <div class = "checkbox">
            <label><input type = "checkbox"> Remember me</label>
         </div>
      </div>
   </div>
   
   <div class = "form-group">
      <div class = "col-sm-offset-2 col-sm-10">
         <button type = "submit" value="Submit" class = "btn btn-default">Sign in</button>
      </div>
   </div>
	
</form>
</body>
</html>