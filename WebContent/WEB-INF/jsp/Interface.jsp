<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Prova</title>
    <link href="../WebContent/bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>
</head>
<body>
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand" src="<%=request.getContextPath()%>/img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				</ul>
				<a class="navbar-brand pull-right" href="<%=request.getContextPath()%>/Login_Action.do">Log In</a>
				
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
</div>

	<div class="panel">
		<div class="container">
			<h1>Login page to the SPECS SLA-Editor application</h1>
			<h2>Please enter your credentials and select your role</h2>
			
			<form:form method="POST" action="/JSPVault/addToken">
   <table>
    <tr>
        <td><form:radiobutton path="key" value="M" />Male <form:radiobutton
					path="key" value="F" />Female</td>
    </tr>
    <tr>
        <td><form:label path="token">Token</form:label></td>
        <td><form:input path="token" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form:form>

		
		
		</div>
	</div>

</body>
</html>