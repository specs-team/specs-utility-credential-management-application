<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Associate to component</title>
    <link href="<c:url value="/bootstrap/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" />
    <style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>
</head>
<body>
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="<c:url value="/img/specs-logo-32.png" />"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/Interface2" >Home</a></li>
					<li><a href="<%=request.getContextPath()%>/WriteSecret" >Write a Secret</a></li>
					<li><a href="<%=request.getContextPath()%>/AssociateToComponent" >Associate an existing component</a></li>
					<li><a href="<%=request.getContextPath()%>/Add_Associate" >Write & associate a component</a></li>
					<li><a href="<%=request.getContextPath()%>/ReadSecret" >Read a Secret</a></li>
					<li><a href="<%=request.getContextPath()%>/Trigger" >Trigger a Component</a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>
<h2>Secret Values</h2>
<form:form method="POST" action="/JSPVault/AssociateToComponent">
   <table>
    <tr>
        <td><form:label path="name">Key Name</form:label></td>
        <td><form:input path="name" /></td>
    </tr>
    <tr>
        <td><form:label path="path">Component Name</form:label></td>
        <td><form:input path="path" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form:form>
</body>
</html>